/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Model.Emprendimiento;
import Model.Producto;
import Model.conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import Test.Conexion;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author leyder
 */
public class ProductoDAO {
    
    Conexion cn = new Conexion();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    int r;
    
    public Producto validar(int id){
        
        Producto pr = new Producto();
        String sql = "select * from `producto` where idProducto= ? ";
        
        try {
            
           con= cn.Conexion();
           ps=con.prepareStatement(sql);
           ps.setInt(1, id);
           
           
           rs=ps.executeQuery();
           
            while (rs.next()) {
                
                pr.setIdProducto(rs.getInt("idProducto"));
                pr.setNombreP(rs.getString("NombreP"));
                pr.setDatos(rs.getString("Datos"));
                pr.setPrecio(rs.getInt("Precio"));
                pr.setCantidad(rs.getInt("cantidad"));
                //em.setId(rs.getInt("id"));
                //em.setNombre(rs.getString("nombre"));
                //em.setCorreo(rs.getString("correo"));
                
                
            }
             
        } catch (Exception e) {
        }
        
        return pr;
    }
    
    
    //OPercaciones CRUD
    
    public List listar(){
        
        String sql = "select * from `producto`";
        List<Producto>lista = new ArrayList<>();
        
        try {
            con=cn.Conexion();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            
            while (rs.next()) {
               Producto pr = new Producto();
               pr.setIdProducto(rs.getInt(1));
               pr.setDatos(rs.getString(2));
               pr.setPrecio(rs.getInt(3));
               pr.setId(rs.getInt(4));
               pr.setCantidad(rs.getInt(5));
               pr.setNombreP(rs.getString(6));
               
               lista.add(pr);
                
            }
        } catch (Exception e) {
        }
        return lista; 
        
        
    }
    
    
    public int agregar(Producto p){
        
        String sql="insert into `producto`values(?,?,?,?,?,?)";
        
        try {
            con=cn.Conexion();
            ps=con.prepareStatement(sql);
            ps.setInt(1, p.getIdProducto());
            ps.setString(2, p.getDatos());
            ps.setInt(3, p.getPrecio());
            ps.setInt(4, p.getId());
            ps.setInt(5, p.getCantidad());
            ps.setString(6, p.getNombreP());
            ps.executeUpdate();
            System.out.println("holaaaaaaaaaaaaaaaaaaaa");
        } catch (Exception e) {
        }
        
        return r;
    }
    
    public Producto listarId(int idproduct){
        Producto p = new Producto();
        String sql = "select * from `producto` where idProducto="+idproduct;
        
        try {
            con=cn.Conexion();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            
            while (rs.next()) {
               
               p.setIdProducto(rs.getInt(1));
               p.setDatos(rs.getString(2));
               p.setPrecio(rs.getInt(3));
               p.setId(rs.getInt(4));
               p.setCantidad(rs.getInt(5));
               p.setNombreP(rs.getString(6));
               
               
                
            }
            
        } catch (Exception e) {
        }
        return p;
    }
    
    public int actualizar(Producto p){
        
        String sql="update `producto` set idProducto=?,Datos=?,Precio=?,id=?,cantidad=?,NombreP=? where idProducto=?";
        
        try {
            con=cn.Conexion();
            ps=con.prepareStatement(sql);
            ps.setInt(1, p.getIdProducto());
            ps.setString(2, p.getDatos());
            ps.setInt(3, p.getPrecio());
            ps.setInt(4, p.getId());
            ps.setInt(5, p.getCantidad());
            ps.setString(6, p.getNombreP());
            ps.setInt(7, p.getIdProducto());
            
            ps.executeUpdate();
            
        } catch (Exception e) {
        }
        
        return r;
        
    }
    
    public void delete(int id){
        
        String sql="delete from `producto` where idProducto="+id;
        
        try {
            con=cn.Conexion();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
            
        } catch (Exception e) {
        }
        
    }
    
    
    
    
}

