/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.ProductoDAO;
import Model.Producto;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author leyder
 */
public class Controlador extends HttpServlet {
    
    Producto pr = new Producto();
    ProductoDAO prDAO= new ProductoDAO();
    int ide;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String menu = request.getParameter("menu");
        String accion = request.getParameter("accion");

        if (menu.equals("Principal")) {
            request.getRequestDispatcher("areasEmpren.jsp").forward(request, response);
        }

        if (menu.equals("Producto")) {
            switch (accion) {

                case "Listar":
                    
                    List lista = prDAO.listar();
                    request.setAttribute("productos", lista);
                    System.out.println(lista);

                    break;
                case "Agregar":
                    
                    String idP = request.getParameter("idProducto");
                    String nombre = request.getParameter("NombreP");
                    String precio = request.getParameter("Precio");
                    String cantidad = request.getParameter("cantidad");
                    String datos = request.getParameter("Datos");
                    
                    int idP1= Integer.parseInt(idP);
                    pr.setIdProducto(idP1);
                    pr.setNombreP(nombre);
                    pr.setDatos(datos);
                    int canti = Integer.parseInt(cantidad);
                    pr.setCantidad(canti);
                    pr.setId(1);
                    int preci = Integer.parseInt(precio);
                    pr.setPrecio(preci);
                    
                    

                    prDAO.agregar(pr);
                    
                    request.getRequestDispatcher("Controlador?menu=Producto&accion=Listar").forward(request, response);
                    System.out.println("hola desde agregar");

                    break;
                case "Editar":
                    
                    ide = Integer.parseInt(request.getParameter("idProducto"));
                    Producto pr = prDAO.listarId(ide);
                    request.setAttribute("producto", pr);

                    break;
                case "Eliminar":

                    break;
                default:
                    throw new AssertionError();
            }
            request.getRequestDispatcher("Producto.jsp").forward(request, response);
        }

        if (menu.equals("Emprendimiento")) {
            request.getRequestDispatcher("Emprendimiento.jsp").forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
